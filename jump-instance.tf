resource "aws_security_group" "jumpSecurity" {
    name= "Jumphost-Security-Group"
    description= "Jump host security group for incoming/outgoing traffic"
    vpc_id= "${aws_vpc.Calendar-vpc.id}"

    egress{
        from_port= 0
        to_port= 0
        protocol= "-1"
        cidr_blocks= ["0.0.0.0/0"]

    }
    ingress{
        from_port= 22
        to_port= 22
        protocol= "tcp"
        cidr_blocks= ["34.204.240.70/32"]
    }

    tags{
        Name= "${var.environment}-JumpHostSecurityGroup"
        Environment = "${var.environment}"
    } 
}

resource "aws_instance" "host" {
    ami= "ami-0080e4c5bc078760e"
    instance_type= "t3.medium"
    subnet_id= "${aws_subnet.jump-subnet.id}"
    iam_instance_profile= "jenkins-node-roles"
    vpc_security_group_ids= ["${aws_security_group.jumpSecurity.id}"]
    root_block_device = {
      volume_type           = "gp2"
      volume_size           = 40
      delete_on_termination = true
    }    
    key_name= "jenkins-server"
    user_data= <<-EOF
                 #!/bin/bash
                 sudo yum update -y
                 sudo yum install docker -y
                 sudo yum install git jq -y
                 sudo service docker start
                 sudo pip install ansible
                 sudo cp /usr/local/bin/ansible /usr/bin/ansible
                 sudo aws s3 cp s3://kubectlbucket0/kubectl_authconfig.sh /tmp
                 sudo aws s3 cp s3://automationbucket00/ssh-hardning.tar.gz /tmp
                 sudo chmod +x /tmp/kubectl_authconfig.sh
                 sudo sh /tmp/kubectl_authconfig.sh
                 sleep 10m
                 sudo aws eks --region us-east-1 update-kubeconfig --name "${var.environment}-${var.ClusterEks}"
                 sudo mkdir /repo
                 sudo curl -o /repo/aws-auth-cm.yaml https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/aws-auth-cm.yaml
                 sudo sed -i "s%<ARN of instance role (not instance profile)>%arn:aws:iam::411185797020:role/Dev-EksWorker%g" /repo/aws-auth-cm.yaml
                 sudo kubectl apply -f /repo/aws-auth-cm.yaml
                 sudo mkdir -p /var/lib/jenkins
                 sudo chown -R ec2-user:ec2-user /var/lib/jenkins
                 sudo yum install java-1.8.0-openjdk.x86_64 -y
                 sudo cp /usr/bin/java /usr/local/bin/java
                 sudo yum remove java-1.7.0-openjdk.x86_64 -y
                 sudo tar -xvzf /tmp/ssh-hardning.tar.gz
                 sudo mkdir ~/.ansible/roles
                 sudo cp -R /tmp/ssh-hardning ~/.ansible/roles
                 sudo ansible-playbook ~/.ansible/roles/ssh-hardning/tests/test.yml
                 EOF

 
    tags{
        Name= "${var.environment}-Jumphost"
        Environment = "${var.environment}"
    } 

}
