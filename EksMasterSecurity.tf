resource "aws_security_group" "EksMasterSecurity" {
    name= "EksMaster-Security-Group"
    description= "Eks Master node security group for incoming/outgoing traffic"
    vpc_id= "${aws_vpc.Calendar-vpc.id}"

    egress{
        from_port= 0
        to_port= 0
        protocol= "-1"
        cidr_blocks= ["0.0.0.0/0"]

    }
    tags{
        Name = "${var.environment}-MasterSecurityGroup"
        Environment = "${var.environment}"
    } 
}
resource "aws_security_group_rule" "eks-cluster-ingress-https" {
  description= "Allow workstation to communicate with kubernetes cluster"
  from_port= 443
  protocol= "tcp"
  security_group_id= "${aws_security_group.EksMasterSecurity.id}"
  source_security_group_id = "${aws_security_group.EksWorkerSecurity.id}"
  to_port= 443
  type= "ingress"
}
