resource "aws_internet_gateway" "internet-gateway" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    
    tags{
        Name= "${var.environment}-${var.internet-gateway}"
        Environment= "${var.environment}"
    }
}
