resource "aws_security_group" "EksWorkerSecurity" {
    name= "${var.environment}-Eksworker"
    description= "Eks worker security group for inbound/outbound"
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    
    egress{
        from_port= 0
        to_port= 0
        protocol= "-1"
        cidr_blocks= ["0.0.0.0/0"]
    }
    tags{
        Name= "${var.environment}-WorkerSecurityGroup"
        Environment= "${var.environment}"
    }
 
}
resource "aws_security_group_rule" "EKS-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.EksWorkerSecurity.id}"
  source_security_group_id = "${aws_security_group.EksWorkerSecurity.id}"
  to_port                  = 65535
  type                     = "ingress"
}
resource "aws_security_group_rule" "EksWorker-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.EksWorkerSecurity.id}"
  source_security_group_id = "${aws_security_group.EksMasterSecurity.id}"
  to_port                  = 65535
  type                     = "ingress"
}
resource "aws_security_group_rule" "EksWorkerpods-ingress-cluster" {
  description              = "Allow pods running extension API servers on port 443 to receive communication from cluster control plane"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.EksWorkerSecurity.id}"
  source_security_group_id = "${aws_security_group.EksMasterSecurity.id}"
  to_port                  = 443
  type                     = "ingress"
}
resource "aws_security_group_rule" "EksWorkerssh-ingress-cluster" {
  description              = "Allow ssh within vpc"
  from_port                = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.EksWorkerSecurity.id}"
  source_security_group_id = "${aws_security_group.jumpSecurity.id}"
  to_port                  = 22
  type                     = "ingress"
}
