#!/bin/bash
echo "Planning infra..."
cd project/infrastructure/
terraform plan -var-file=properties/dev-variables.tfvars -out=plan_data

if [ $? != 0 ]
then
 echo "Planning is done!!"
else
 echo "planning failed."
fi
