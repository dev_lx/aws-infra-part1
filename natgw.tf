resource "aws_nat_gateway" "nat-gw" {
  count= "${length(var.nat-count)}"
  allocation_id= "${aws_eip.eip.id}"
  subnet_id= "${element(aws_subnet.nat-subnet.*.id, count.index)}"
  tags{
      Name= "${var.environment}-Natgateway-${count.index}"
      }
  }
