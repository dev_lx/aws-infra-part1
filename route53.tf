
resource "aws_route53_zone" "private" {
  name="dev.calendar.com"
 

  vpc {
    vpc_id = "${aws_vpc.Calendar-vpc.id}"
  }
  tags{
    Name= "${var.environment}-privatezone"
    Environment= "${var.environment}"
  }
}
