resource "aws_security_group" "allow_redis_traffic" {
  name        = "allow_redis"
  description = "Allow redis traffic"
  vpc_id      = "${aws_vpc.Calendar-vpc.id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.cidr}"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_elasticache_cluster" "redis-cluster" {
  cluster_id= "mycluster"
  replication_group_id= "${aws_elasticache_replication_group.replication-group.id}"  
  tags{
    Name= "${var.environment}-rediscluster" 
    Environment= "${var.environment}"
  }
}
resource "aws_elasticache_subnet_group" "redis-subnet-group" {
  name= "${var.environment}-redis-subnet-group"
  subnet_ids= ["${aws_subnet.redis-subnet.*.id}"]
  
}
resource "aws_elasticache_replication_group" "replication-group" {
    automatic_failover_enabled= true
    availability_zones= "${var.availability_zones}"
    replication_group_id= "redis-replica"
    replication_group_description= "redis replica group"
    node_type= "cache.t2.micro"
    number_cache_clusters= 3
    security_group_ids = [ "${aws_security_group.allow_redis_traffic.id}" ] 
    port= 6379
    subnet_group_name= "${aws_elasticache_subnet_group.redis-subnet-group.name}"
    
    tags{
        Name= "${var.environment}-Elastic_replication_group"
        Environment= "${var.environment}"
    } 
}

