data "aws_region" "current" {}
locals {
    eks-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
sudo yum update -y
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.cluster.certificate_authority.0.data}' '${var.environment}-${var.ClusterEks}'
pip install ansible
sudo mkdir ~/.ansible/roles
sudo aws s3 cp s3://automationbucket00/ssh-hardning.tar.gz /tmp
sudo tar -xvzf /tmp/ssh-hardning.tar.gz -C ~/.ansible/roles/
ansible-playbook ~/.ansible/roles/ssh-hardning/tests/test.yml
echo $? > /usr/local/src/data_ansible
USERDATA
}

resource "aws_launch_configuration" "eks_launch" {
    image_id= "${data.aws_ami.eks.id}"
    iam_instance_profile= "${aws_iam_instance_profile.Eks-worker.name}"
    instance_type= "${var.instance_type}"
    security_groups= ["${aws_security_group.EksWorkerSecurity.id}"]
    user_data_base64= "${base64encode(local.eks-node-userdata)}"
    key_name= "staging-eks-workers"
    lifecycle{
        create_before_destroy= true
    }
}
