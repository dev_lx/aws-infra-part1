#!/bin/bash
echo "Building infra..."
cd project/infrastructure/
terraform plan -var-file=properties/dev-variables.tfvars -out=apply
terraform apply -var-file=properties/dev-variables.tfvars -auto-approve 
echo "Dev infra is up"
