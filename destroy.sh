#!/bin/bash
echo "Destroying infra..."
cd project/infrastructure/
terraform destroy -var-file=properties/dev-variables.tfvars -auto-approve 
