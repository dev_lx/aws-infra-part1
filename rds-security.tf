resource "aws_security_group" "rds-security" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
      
    ingress{
        from_port= 5432
        to_port= 5432
        protocol= "tcp"
        cidr_blocks= ["${var.cidr}"]
    }
     egress{
        from_port= 80
        to_port= 80
        protocol= "tcp"
        cidr_blocks= ["0.0.0.0/0"]
    }
     egress{
        from_port= 443
        to_port= 443
        protocol= "tcp"
        cidr_blocks= ["0.0.0.0/0"]
    }
    egress{
        from_port= 5434
        to_port= 5434
        protocol= "tcp"
        cidr_blocks= ["0.0.0.0/0"]
    }
}
