locals {
  # These represent dynamic list of ecr to create
  ecr = "${var.ecrNames}"
}

resource "aws_ecr_repository" "repository" {
  count = "${length(local.ecr)}"
  name = "${element(local.ecr, count.index)}"
  tags{
   Name= "${element(local.ecr, count.index)}"
   Envoirment= "${var.environment}"
}
}
