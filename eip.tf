
resource "aws_eip" "eip" {
  vpc= "true"
  tags{
    Name= "${var.environment}-eip"
    Environment= "${var.environment}"
  }
}
resource "aws_eip" "jumphost" {
  instance = "${aws_instance.host.id}"
  vpc      = true
  tags{
    Name= "${var.environment}-jumphost"
    Environment= "${var.environment}"
  }

}
