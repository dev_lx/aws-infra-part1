/*
 * Bucket for ansible role
 */

resource "aws_s3_bucket" "bucket" {
  bucket = "automationbucket00"
  acl    = "private"

  tags = {
    Name        = "${var.environment}-Ansible"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.bucket.id}"
  key    = "ssh-hardning.tar.gz"
  source = "ssh-hardning.tar.gz"
 }

/*
 * Bucket for kubectl
 */
resource "aws_s3_bucket" "bucket0" {
  bucket = "kubectlbucket0"
  acl    = "private"
  versioning {
    enabled = true
  }

  tags = {
    Name        = "${var.environment}-Kubectl"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket_object" "object0" {
  bucket = "${aws_s3_bucket.bucket0.id}"
  key    = "kubectl_authconfig.sh"
  source = "kubectl_authconfig.sh"
 }


/*
 * Website Bucket (calendar.com)
 */

resource "aws_s3_bucket" "web" {
  bucket = "dev0.calendar.com"
  acl = "public-read"
  website {
    redirect_all_requests_to = "https://www.dev.calendar.com"
  }
  tags{
    Name= "${var.environment}-redirect-web"
    Enviroment= "${var.environment}"
}
}

