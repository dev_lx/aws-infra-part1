#!/bin/bash
echo "Building infra..."
cd project/infrastructure/
terraform plan -var-file=properties/dev-variables.tfvars -out=plan_data
terraform apply -auto-approve "plan_data" 
