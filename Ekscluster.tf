resource "aws_eks_cluster" "cluster" {
    name= "${var.environment}-${var.ClusterEks}"
    role_arn= "${aws_iam_role.Master-Role.arn}"
    version= "1.10"

    vpc_config{
        security_group_ids= ["${aws_security_group.EksMasterSecurity.id}"]
        subnet_ids= ["${aws_subnet.eks-cluster-private.*.id}", "${aws_subnet.public-eks.*.id}"]
    }  
    depends_on= [
        "aws_iam_role_policy_attachment.Master-Role-AmazonEKSClusterPolicy",
        "aws_iam_role_policy_attachment.Master-Role-AmazonEKSServicePolicy"
        
    ]
}
output "endpoint" {
  value = "${aws_eks_cluster.cluster.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${aws_eks_cluster.cluster.certificate_authority.0.data}"
}
