#!/bin/bash
echo "Initializing & Planning...."
cd test-demo/infrastructure/
terraform init
terraform plan
echo "Planning is done"
