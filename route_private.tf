resource "aws_route_table" "route-tb-private" {
  vpc_id= "${aws_vpc.Calendar-vpc.id}"

  route{
      cidr_block= "0.0.0.0/0"
      nat_gateway_id= "${aws_nat_gateway.nat-gw.id}"
  }
  tags{
      Name= "${var.environment}-Private-Table"
      Environment= "${var.environment}"
      
  }
}
resource "aws_route_table_association" "eks-cluster-subnet-association" {
    count= "${length(var.eksc)}"
    subnet_id= "${element(aws_subnet.eks-cluster-private.*.id, count.index)}"
    route_table_id= "${aws_route_table.route-tb-private.id}"
  
}
resource "aws_route_table_association" "eks-worker-subnet-association" {
    count= "${length(var.eksw)}"
    subnet_id= "${element(aws_subnet.eks-worker.*.id, count.index)}"
    route_table_id= "${aws_route_table.route-tb-private.id}"
  
}

