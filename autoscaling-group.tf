resource "aws_autoscaling_group" "eksgroup" {
    desired_capacity= 2
    launch_configuration= "${aws_launch_configuration.eks_launch.id}"
    max_size= 3
    min_size= 1
    name= "Workernode"
    vpc_zone_identifier= ["${aws_subnet.eks-worker.*.id}"]

    tag {
      key= "Name"
      value= "${var.environment}-WorkerNode"
      propagate_at_launch = true
    }
    

  tag {
  
    key                 = "kubernetes.io/cluster/${var.environment}-${var.ClusterEks}"
    value               = "owned"
    propagate_at_launch = true
   
  }
  
  
}
