resource "aws_iam_role" "Eks-worker" {
  name = "${var.environment}-EksWorker"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "Eks-worker-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.Eks-worker.name}"
}

resource "aws_iam_role_policy_attachment" "Eks-worker-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.Eks-worker.name}"
}

resource "aws_iam_role_policy_attachment" "Eks-worker-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.Eks-worker.name}"
}

resource "aws_iam_role_policy_attachment" "AmazonSQSFullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = "${aws_iam_role.Eks-worker.name}"
}
resource "aws_iam_role_policy_attachment" "AmazonS3ReadOnlyAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  role       = "${aws_iam_role.Eks-worker.name}"
}



resource "aws_iam_instance_profile" "Eks-worker" {
  name = "terraform-eks-demo"
  role = "${aws_iam_role.Eks-worker.name}"
}


