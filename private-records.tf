resource "aws_route53_record" "www" {
  zone_id = "${var.public_zone_id}"
  name    = "dev.calendar.com"
  type    = "A"
  alias {
    name                   = "${aws_cloudfront_distribution.www_distribution.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.www_distribution.hosted_zone_id}"
    evaluate_target_health = false
  }
}
resource "aws_route53_record" "webappcdn" {
  zone_id = "${var.public_zone_id}"
  name    = "www"
  type    = "A"
  alias {
    name                   = "${aws_cloudfront_distribution.webapp_distribution.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.webapp_distribution.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "rds" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "db"
  type    = "CNAME"
  ttl     = "60"

  records        = ["${aws_db_instance.db.address}"]
}

resource "aws_route53_record" "redis" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "redis1"
  type    = "CNAME"
  ttl     = 60
  records = ["${aws_elasticache_cluster.redis-cluster.cache_nodes.0.address}"]
}

resource "aws_route53_record" "redis1" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "redis"
  type    = "CNAME"
  ttl     = 60
  records = ["${aws_elasticache_replication_group.replication-group.primary_endpoint_address}"]
}








