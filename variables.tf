variable "vpc" {
  default= "calendar"
  
}

variable "cidr" {
  type= "string"
 
}
variable "availability_zones" {
    type= "list"  
}
variable "internet-gateway" {
  default= "Gateway"
  
}

variable "natgw" {
  description= "Nat gateway subnets"
  type= "list"
  
}
variable "elb_public" {
  description= "Elb public subnets"
  type= "list"
  
}
variable "jump-host" {
  description= "Jump host subnets"
  type= "list"
  
}
variable "rds" {
  description= "Rds subnets"
  type= "list"
  
}
variable "redis" {
  description= "Redis subnets"
  type= "list"
  
}
variable "eksc" {
  description= "Eks cluster in private subnets"
  type= "list"
  
}
variable "ekscluster" {
  description= "Eks cluster in public subnets"
  type= "list"
  
}
variable "eksw" {
  description= "Eks worker subnets"
  type= "list"
  
}
variable "elb_private" {
  description= "Elb Private subnets"
  type= "list"
  
}

variable "nat-count" {
  description= "No. of natgatway"
  default= 2

  
}
variable "ClusterEks" {
  description= "Eks cluster name"
  default= "Calendar-cluster"
  
}

variable "instance_type" {
  description= "instance type for ec2 instance"
  default= "t2.medium"
  
}

variable "capacity" {
  default= 2
  
}

variable "environment"{}
variable "account_id"{}
variable "db_password"{}

variable "ecrNames" {
  type="list"
  default=[]
  description="ecr names for services"
}
variable "webappELB" {
  type= "string"
}
variable "public_zone_id" {
  type= "string"
}
