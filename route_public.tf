resource "aws_route_table" "route-tb" {
  vpc_id= "${aws_vpc.Calendar-vpc.id}"

  route{
      cidr_block= "0.0.0.0/0"
      gateway_id= "${aws_internet_gateway.internet-gateway.id}"
  }
  tags{
      Name= "${var.environment}-Public-Table"
       Environment= "${var.environment}"
  }
}

/*
 * Route-Table association
 */
resource "aws_route_table_association" "Nat-subnet-association" {
      count= "${length(var.natgw)}"
      subnet_id= "${element(aws_subnet.nat-subnet.*.id, count.index)}"
      route_table_id= "${aws_route_table.route-tb.id}"
}

resource "aws_route_table_association" "JumpHost-subnet-association" {
      count= "${length(var.jump-host)}"
      subnet_id= "${element(aws_subnet.jump-subnet.*.id, count.index)}"
      route_table_id= "${aws_route_table.route-tb.id}"
}
resource "aws_route_table_association" "Eks-public-subnet-association" {
      count= "${length(var.ekscluster)}"
      subnet_id= "${element(aws_subnet.public-eks.*.id, count.index)}"
      route_table_id= "${aws_route_table.route-tb.id}"
}



