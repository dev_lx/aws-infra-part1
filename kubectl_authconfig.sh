#!/bin/bash
sudo yum update -y
curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.13/2019-03-27/bin/linux/amd64/kubectl
sudo chmod +x ./kubectl
sudo mv ./kubectl /usr/bin/kubectl
curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.7/2019-03-27/bin/linux/amd64/aws-iam-authenticator
sudo chmod +x ./aws-iam-authenticator
sudo mv ./aws-iam-authenticator /usr/bin/aws-iam-authenticator
