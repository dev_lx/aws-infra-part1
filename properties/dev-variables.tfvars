environment = "Dev"
account_id = ""
db_password = ""
public_zone_id = "" 
webappELB=""
ecrNames = ["dev-calendar/api", "dev-calendar/gateway", "dev-calendar/authentication", "dev-calendar/transcription", "dev-calendar/caching", "dev-calendar/webapp", "dev-calendar/sync-third-party-events-producer", "dev-calendar/transcription-cron", "dev-calendar/refresh-token", "dev-calendar/reminder-notifications", "dev-calendar/email", "dev-calendar/renew-push-notification-subscription", "dev-calendar/sync-third-party-events-consumer" ]
