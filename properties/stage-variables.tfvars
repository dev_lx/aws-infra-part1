environment= "staging"
account_id=""
db_password=
ecrNames = ["stage-calendar/api", "stage-calendar/gateway", "stage-calendar/authentication", "stage-calendar/transcription", "stage-calendar/caching", "stage-calendar/webapp", "stage-calendar/sync-third-party-events-producer", "stage-calendar/transcription-cron", "stage-calendar/refresh-token", "stage-calendar/reminder-notifications", "stage-calendar/email", "stage-calendar/renew-push-notification-subscription", "stage-calendar/sync-third-party-events-consumer" ]
