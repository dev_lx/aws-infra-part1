/*
 * Virtual Private Cloud
 */

resource "aws_vpc" "Calendar-vpc" {
  cidr_block= "${var.cidr}"
  enable_dns_hostnames = true
  tags{
      Name= "${var.environment}-${var.vpc}" 
      Enviroment= "${var.environment}"
      key= "kubernetes.io/cluster/${var.environment}-${var.ClusterEks}" 
      value= "shared"   
 }
}

/*
 * Public-Subnets
 */
 resource "aws_subnet" "nat-subnet" {
  vpc_id= "${aws_vpc.Calendar-vpc.id}"
  count= "${length(var.natgw)}"
  cidr_block= "${element(var.natgw, count.index)}"
  availability_zone= "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch= "true"

  tags{
      Name= "${var.environment}-Natsubnet-${count.index}"
      Environment= "${var.environment}"
  }
}

resource "aws_subnet" "jump-subnet" {
  vpc_id= "${aws_vpc.Calendar-vpc.id}"
  count= "${length(var.jump-host)}"
  cidr_block= "${element(var.jump-host, count.index)}"
  availability_zone= "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch= "true"

  tags{
      Name= "${var.environment}-JumpHost-${count.index}"
      Environment= "${var.environment}"
  }
}
resource "aws_subnet" "public-eks" {
  vpc_id= "${aws_vpc.Calendar-vpc.id}"
  count= "${length(var.ekscluster)}"
  cidr_block= "${element(var.ekscluster, count.index)}"
  availability_zone= "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch= "true"

  tags{
      Name= "${var.environment}-EksClusterPublic"
      Environment= "${var.environment}"
      "kubernetes.io/cluster/Dev-Calendar-cluster"= "shared"
      "kubernetes.io/role/elb"= 1
  }
}
/*
 * Private-Subnets
 */
resource "aws_subnet" "rds-subnet" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    count= "${length(var.rds)}"
    cidr_block= "${element(var.rds, count.index)}"
    availability_zone= "${element(var.availability_zones, count.index)}"

    tags{
       Name= "${var.environment}-Rds-${count.index}"
       Environment= "${var.environment}"
    }

}
resource "aws_subnet" "redis-subnet" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    count= "${length(var.redis)}"
    cidr_block= "${element(var.redis, count.index)}"
    availability_zone= "${element(var.availability_zones, count.index)}"
    tags{
      Name= "${var.environment}-Redis-${count.index}"
      Environment= "${var.environment}"
    }

}
resource "aws_subnet" "eks-cluster-private" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    count= "${length(var.eksc)}"
    cidr_block= "${element(var.eksc, count.index)}"
    availability_zone= "${element(var.availability_zones, count.index)}" 
    tags{
      Name= "${var.environment}-EksClusterPrivate"
      Environment= "${var.environment}"
      "kubernetes.io/cluster/Dev-Calendar-cluster"= "shared"
    }

}
resource "aws_subnet" "eks-worker" {
    vpc_id= "${aws_vpc.Calendar-vpc.id}"
    count= "${length(var.eksw)}"
    cidr_block= "${element(var.eksw, count.index)}"
    availability_zone= "${element(var.availability_zones, count.index)}" 
    tags{
      Name= "${var.environment}-EksWorker"
      Environment= "${var.environment}"
      key= "kubernetes.io/cluster/${var.environment}-${var.ClusterEks}" 
      value= "shared"
    }
}
