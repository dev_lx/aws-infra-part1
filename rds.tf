resource "aws_db_subnet_group" "rds-subnet-grp" {
  name       = "main"
  subnet_ids = ["${aws_subnet.rds-subnet.*.id}"]

  tags = {
    Name = "${var.environment}-RdsSecurityGroup"
    Environment= "${var.environment}"
  }
}
resource "aws_db_instance" "db" {
    identifier= "calendar-dev"
    allocated_storage= 20
    storage_type= "gp2"
    engine= "postgres"
    engine_version= "10.6"
    instance_class= "db.t2.medium"
    name= "calendar_auth"
    username= "calendar"
    password= "${var.db_password}"
    publicly_accessible      = false
    storage_encrypted        = true # you should always do this
    storage_type             = "gp2"
    db_subnet_group_name= "${aws_db_subnet_group.rds-subnet-grp.name}"
    vpc_security_group_ids= ["${aws_security_group.rds-security.id}"]
    skip_final_snapshot= true
    tags{
      Name= "calendar-${var.environment}"
      Environment = "${var.environment}"
    }
}





