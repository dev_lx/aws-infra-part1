resource "aws_sqs_queue" "sqs-service" {
  name= "${var.environment}-calendar.fifo"
  delay_seconds= 0
  max_message_size= 2048
  message_retention_seconds= 360
  receive_wait_time_seconds= 10
  fifo_queue                  = true
  content_based_deduplication = true
  tags = {
    Name= "${var.environment}-calendar.fifo"
    Environment = "${var.environment}"
  }
}
resource "aws_sqs_queue" "event-sqs-service" {
  name= "${var.environment}-calendar-event.fifo"
  delay_seconds= 0
  max_message_size= 256000
  message_retention_seconds= 360
  receive_wait_time_seconds= 0
  fifo_queue                  = true
  content_based_deduplication = true
  tags = {
    Name= "${var.environment}-calendar-event.fifo"
    Environment = "${var.environment}"
  }
}

resource "aws_sqs_queue_policy" "allow-account" {
  queue_url = "${aws_sqs_queue.event-sqs-service.id}"

  policy = <<POLICY
  {
  "Version": "2012-10-17",
  "Id": "sqspolicy1",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": { 
	"AWS": [
	    "arn:aws:iam::${var.account_id}:root"
        ]
      },
      "Action": "sqs:*",
      "Resource": "${aws_sqs_queue.event-sqs-service.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sqs_queue.event-sqs-service.arn}"
        }
    }
   }
  ]
  }
  POLICY
}

resource "aws_sqs_queue_policy" "allow-account-1" {
  queue_url = "${aws_sqs_queue.sqs-service.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy2",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": { 
	"AWS": [
	    "arn:aws:iam::${var.account_id}:root"
        ]
      },
      "Action": "sqs:*",
      "Resource": "${aws_sqs_queue.sqs-service.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sqs_queue.sqs-service.arn}"
        }
      }
    }
  ]
}
POLICY
}

 
