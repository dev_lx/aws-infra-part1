output "EksCluster-Name" {
  value= "${aws_eks_cluster.cluster.name}"
}

output "workerid" {
  value= "${data.aws_ami.eks.name}"
}

output "aws_auth_worker" {
  value = "${local.config_map_aws_auth}"
}

output "aws_kubeconfig" {
  value = "${local.kubeconfig}"
}
